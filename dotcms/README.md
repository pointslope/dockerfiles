# dotCMS

[dotCMS](http://dotcms.com/) container for exploring it's capabilities.

Built according to the dotCMS
[installation instructions](http://dotcms.com/docs/latest/InstallingFromRelease).

## How to run

1. Provision a database container such as
   [postgres](https://registry.hub.docker.com/_/postgres/) with the
   name `db`.

        docker run -d -p 5432:5432 --name db postgres

2. Create a `dotcms db provisioner` container linked to the `db` container. This container will
   create the dotcms db and user. The image can be built from the Dockerfile in the `db` directory.

        docker run --rm --link db:db dotcms-db-provisioner

3. Create a `dotcms` container linked to the `db` container.

        docker run -d -p 8080:80 \
           -v /dotcms/config:/opt/dotcms/dotserver/tomcat/conf/Catalina/localhost \
           --name dotcms \
           --link db:db dotcms

4. From the [admin](http://localhost:8080/admin) page, login with the
   username/password admin@dotcms.com/admin.
