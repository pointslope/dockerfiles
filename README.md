# Dockerfiles

This repository contains the [Dockerfiles](http://docker.io) for Point Slope, LLC
[trusted builds](https://registry.hub.docker.com/repos/pointslope/).

Want to suggest an improvement? Fork and issue a pull request!

## Docker host

A generic docker host vagrant file is included. It's based on
boot2docker but includes VB guest additions for folder sync'ing
support. container-specific port and synced folder configurations are
supported via a `vagrant_config.yml`file. These files should be stored
in the respective container source directory.

Note: Version 1.2 of Docker uses the [newly registered](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=docker) IANA port 2375.
