require 'yaml'

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  vconfig =  File.exists?('vagrant_config.yml') ? YAML::load_file('vagrant_config.yml') : {}

  config.vm.box = "yungsang/boot2docker"

  config.vm.provider "virtualbox" do |v|
    v.name = "dockerhost"
    v.memory = 4096
    v.cpus = 2
  end
  
  # Shell provisioner runs as root by default
  config.vm.provision :shell, inline: "echo DOCKER_OPTS=\"-H=0.0.0.0:2375\" > /etc/default/docker"
  config.vm.provision :shell, inline: "/etc/init.d/docker restart"

  # Configure port forwarding of Docker host to local system
  (49000..49900).each do |port|
    config.vm.network "forwarded_port", guest: port, host: port, autocorrect: true
  end if ENV['DOCKER_PORTMAP'].eql?("1")
  
  # map project-specific ports
  port_mappings = vconfig['port_mappings'] || {}

  port_mappings.each do |p|
    config.vm.network "forwarded_port", guest: p['guest'], host: p['host']
  end

  # sync project-specific folders
  synced_folders = vconfig['synced_folders'] || {}

  synced_folders.each do |s|
    config.vm.synced_folder s['host'], s['guest']
  end
end
