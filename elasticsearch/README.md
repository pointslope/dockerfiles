# pointslope/elasticsearch

This Docker image builds on `pointslope/openjdk7` and adds the latest release
of [Elastic Search](http://www.elasticsearch.org/overview/elasticsearch/).

## Notes

This image exposes the following `ENV` vars for your customization pleasure:

`ENV ES_HEAP_SIZE 512m`
`ENV ES_JAVA_OPTS -Djava.awt.headless=true`
`ENV ES_LOG_LEVEL DEBUG`
`ENV ES_CONFIG /etc/elasticsearch/elasticsearch.yml`

Changing these values on *docker run* allows you to perform just about
any configuration change you may desire.

You will also likely want to mount some volumes for the Elastic Search
**LOG_DIR** (default /var/log/elasticsearch) and/or
**DATA_DIR** (default /var/data/elasticsearch). See the Elastic Search
[documentation](http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/setup-configuration.html) for more info.

You will also want to change the default Elastic Search configuration
which you can do by basing your Docker image on this one and **ADD**ing
your configuration file at the path given by **ES_CONFIG**, for instance.

## MIT License 

Copyright (c) 2014 Point Slope, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
